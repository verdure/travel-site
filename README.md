# Travel Site

This is a basic travel-themed website. It features a user authentication system,
a forum where users can post questions, a webstore, and basic photo gallery pages.

It was developed using the Django web framework.