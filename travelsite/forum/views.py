from django.shortcuts import render
from django.views.generic import ListView, CreateView, DetailView, DeleteView
from forum.models import ForumPost, Comment
from forum.forms import PostForm, CommentForm
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import forms
from django.urls import reverse, reverse_lazy

class ForumPostListView(ListView):
    model = ForumPost
    template_name = 'forumpost_list.html'
    paginate_by = 15

class ForumPostCreateView(LoginRequiredMixin, CreateView):
    """docstring for ForumPostCreateView."""
    model = ForumPost
    fields = ('title', 'post_text')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return super().form_valid(form)

class ForumPostDetailView(DetailView):
    model = ForumPost

class ForumPostDeleteView(LoginRequiredMixin, DeleteView):
    model = ForumPost
    success_url = reverse_lazy('forum:post_list')

class CommentCreateView(LoginRequiredMixin, CreateView):
    """docstring for CommentCreateView."""
    model = Comment
    fields = ('comment_text')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return super().form_valid(form)

class CommentDeleteView(LoginRequiredMixin, DeleteView):
    model = Comment
    success_url = reverse_lazy('forum:post_detail')
