from django.views.generic.base import TemplateView

class HomePageView(TemplateView):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

class AboutView(TemplateView):
    template_name = 'about.html'

class PlacesView(TemplateView):
    template_name = 'places/places.html'
